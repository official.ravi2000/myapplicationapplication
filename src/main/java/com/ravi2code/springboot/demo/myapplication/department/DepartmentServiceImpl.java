package com.ravi2code.springboot.demo.myapplication.department;

import com.ravi2code.springboot.demo.utils.DepartmentEnum;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl implements DepartmentService{


    public String getDepartments() {
         return  DepartmentEnum.CR.name()+" "+DepartmentEnum.FB.getDisplayName();
    }
}
