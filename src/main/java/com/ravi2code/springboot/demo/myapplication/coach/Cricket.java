package com.ravi2code.springboot.demo.myapplication.coach;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
public class Cricket implements  Game{
    @Override
    public String getWorkOut() {
        return "Play <b style=\"color:blue\"> Cricket </b>";
    }
}
