package com.ravi2code.springboot.demo.myapplication.coach;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CoachImpl implements Coach{


    //injecting properties using @Value annotation
    @Value("${coach.name}")
    private String coachName;

    @Value("${team.name}")
    private String teamName;

    @Override
    public String getWorkout() {


        return "<div>\n  " +
                "        <label>Coach Name:</label>\n" +
                "        <span style=\"color: red;\">"+coachName+"</span>\n" +
                "    </div>\n" +
                "    <div>\n" +
                "        <label>Team Name:</label>\n" +
                "        <span style=\"color: red;\">"+teamName+"</span>\n" +
                "    </div>";
    }
}
