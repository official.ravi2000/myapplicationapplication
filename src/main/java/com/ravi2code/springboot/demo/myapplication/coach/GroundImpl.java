package com.ravi2code.springboot.demo.myapplication.coach;

import org.springframework.stereotype.Service;

@Service
public class GroundImpl implements Ground{
    @Override
    public String groundName() {
        return "We are in <b style=\"color:red\"> Vankhede </b>Stadium";
    }

}
