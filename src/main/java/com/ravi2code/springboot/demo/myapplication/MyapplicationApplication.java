package com.ravi2code.springboot.demo.myapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.*;
import org.springframework.boot.web.servlet.support.*;

@SpringBootApplication(
		scanBasePackages = "com.ravi2code.springboot.demo,com.ravi2code.springboot.demo.utils")


//@SpringBootApplication
public class MyapplicationApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		//this is out springboot applicationx
		SpringApplication.run(MyapplicationApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MyapplicationApplication.class);
	}
}
