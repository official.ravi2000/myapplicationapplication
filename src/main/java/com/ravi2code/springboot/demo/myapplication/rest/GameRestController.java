package com.ravi2code.springboot.demo.myapplication.rest;

import com.ravi2code.springboot.demo.myapplication.Commity.CommityService;
import com.ravi2code.springboot.demo.myapplication.Commity.Details;
import com.ravi2code.springboot.demo.myapplication.Franchise.FranchiseService;
import com.ravi2code.springboot.demo.myapplication.coach.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameRestController {

    @Autowired
    @Qualifier("cricket")
    private Game game;

    @Autowired
    private FranchiseService franchiseService;

    @Autowired
    private CommityService commityService;

    @Autowired
    private Details details;

    @GetMapping("/getGame")
    public String getGame(){
        /**
        Doing something with the data
         */
        return game.getWorkOut();
    }

    @GetMapping("/getFranchise")
    public  String getFranchise(){
        return franchiseService.getFranchise();
    }

    @GetMapping("/getCommity")
    public  String getCommity(){
        return commityService.getCommity();
    }


    @GetMapping("/getDetails")
    public  String getDetails(){
        return details.getDetails();
    }
}
