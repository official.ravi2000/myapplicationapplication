package com.ravi2code.springboot.demo.myapplication.Commity;

import com.ravi2code.springboot.demo.utils.CommityBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommityServiceImpl implements  CommityService {

    @Autowired
    private CommityBean commityBean;


    @Override
    public String getCommity() {
        return commityBean.toString();
    }
}
