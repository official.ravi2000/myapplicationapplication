package com.ravi2code.springboot.demo.myapplication.Franchise;

import com.ravi2code.springboot.demo.utils.Franchise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FranchiseServiceImpl implements FranchiseService{
    @Autowired
    private  Franchise franchise;

    @Override
    public String getFranchise() {
        return franchise.toString();
    }
}
