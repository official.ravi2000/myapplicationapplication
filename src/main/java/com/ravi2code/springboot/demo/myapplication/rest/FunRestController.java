package com.ravi2code.springboot.demo.myapplication.rest;

import com.ravi2code.springboot.demo.myapplication.coach.Coach;
import com.ravi2code.springboot.demo.myapplication.coach.Ground;
import com.ravi2code.springboot.demo.myapplication.department.DepartmentService;
import com.ravi2code.springboot.demo.utils.Commans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
//restController annotation
@RestController
public class FunRestController {


    Coach coach;
    DepartmentService departmentService;

    @Autowired //Contructor injection
    public FunRestController(Coach coach){
        this.coach=coach;
    }

    @Autowired
    public void setDepartmentService(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    //Field injection
    @Autowired
    private Ground ground;


    //we are going to display this message on web
    @GetMapping("/")
    public String sayHello(){
        return "Hello Ravindra Baranjalekar";
    }

    @GetMapping("/getworkout")
    public String getDailyWorkOut(){
        return "<br><b><span style=\"color:red\">Run a hard 5..!</span></b> "+ Commans.DEPARTMENT;
    }


    @GetMapping("/getProperties")
    public String getProperties(){
        return  coach.getWorkout();
    }

    @GetMapping("/getStadiumName")
    public String getStadiumName(){
        return ground.groundName();
    }

    @GetMapping("/getDepartments")
    public String getDepartment(){
        return departmentService.getDepartments();
    }

}
