package com.ravi2code.springboot.demo.myapplication.coach;

public interface Coach {
    public String getWorkout();
}
