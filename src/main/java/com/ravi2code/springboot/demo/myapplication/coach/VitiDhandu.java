package com.ravi2code.springboot.demo.myapplication.coach;

import org.springframework.stereotype.Component;

@Component
public class VitiDhandu implements Game{
    @Override
    public String getWorkOut() {
        return "Play <b style=\"color:blue\"> VitiDandu </b>";
    }
}
