package com.ravi2code.springboot.demo.myapplication.Commity;

import com.ravi2code.springboot.demo.utils.CommityBean;
import com.ravi2code.springboot.demo.utils.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailsImpl implements  Details {

    @Autowired
    private Employee employee;


    @Override
    public String getDetails() {
        return employee.toString();
    }
}
