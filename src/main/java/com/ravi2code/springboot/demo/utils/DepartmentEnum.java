package com.ravi2code.springboot.demo.utils;


public enum DepartmentEnum {
    CR("CR","Cricket"),FB("FB","FootBool");

    private String statusCode;
    private String displayName;

    DepartmentEnum(String statusCode, String displayName) {
        this.statusCode=statusCode;
        this.displayName=displayName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getDisplayName() {
        return displayName;
    }
}
