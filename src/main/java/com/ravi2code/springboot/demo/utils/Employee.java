package com.ravi2code.springboot.demo.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@ConfigurationProperties(prefix = "emp.info")
public class Employee {

    /**
     * Array Property
     */
    private String[] favColors;

    /**
     * Collection Type
     */
    private List<String> nickNames;
    private Set<String> phoneNumbers;
    private Map<String,Object> idDetails;

    private Company company;

    public String[] getFavColors() {
        return favColors;
    }

    public void setFavColors(String[] favColors) {
        this.favColors = favColors;
    }

    public List<String> getNickNames() {
        return nickNames;
    }

    public void setNickNames(List<String> nickNames) {
        this.nickNames = nickNames;
    }

    public Set<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(Set<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Map<String, Object> getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(Map<String, Object> idDetails) {
        this.idDetails = idDetails;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Favorite Colors: ");
        if (favColors != null && favColors.length > 0) {
            sb.append(Arrays.toString(favColors));
        } else {
            sb.append("N/A");
        }
        sb.append("\n");

        sb.append("Nicknames: ");
        if (nickNames != null && !nickNames.isEmpty()) {
            sb.append(nickNames);
        } else {
            sb.append("N/A");
        }
        sb.append("\n");

        sb.append("Phone Numbers: ");
        if (phoneNumbers != null && !phoneNumbers.isEmpty()) {
            sb.append(phoneNumbers);
        } else {
            sb.append("N/A");
        }
        sb.append("\n");

        sb.append("ID Details: ");
        if (idDetails != null && !idDetails.isEmpty()) {
            sb.append(idDetails);
        } else {
            sb.append("N/A");
        }
        sb.append("\n");

        sb.append("Company: ");
        if (company != null) {
            sb.append(company);
        } else {
            sb.append("N/A");
        }

        return sb.toString();
    }

}
