package com.ravi2code.springboot.demo.utils;


public class Company {
    private  Integer id;
    private  String name;
    private String  addes;

    public Company(Integer id, String name, String addes) {
        this.id = id;
        this.name = name;
        this.addes = addes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddes() {
        return addes;
    }

    public void setAddes(String addes) {
        this.addes = addes;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", addes='" + addes + '\'' +
                '}';
    }
}
